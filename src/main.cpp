/*
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QCommandLineParser>
#include <QStandardPaths>
#include <QIcon>

#include <KAboutData>
#include <KLocalizedString>
#include <KFilePlacesModel>
#include <KLocalizedContext>
#include <KLocalizedString>

#include "dirmodel.h"
#include "dirmodelutils.h"
#include "dirlister.h"
#include "fileplacesmodel.h"

int main(int argc, char **argv)
{

    QApplication app(argc, argv);
    app.setApplicationDisplayName("Whale");
    app.setOrganizationDomain("kde.org");
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("whale")));
    
    KLocalizedString::setApplicationDomain("whale");
    
    KAboutData aboutData(QStringLiteral("whale"),
                         xi18nc("@title", "<application>Whale</application>"),
                         QStringLiteral("0.1-dev"),
                         xi18nc("@title", "Whale is a file manager."),
                         KAboutLicense::GPL,
                         xi18nc("@info:credit", "(c) 2020 KDE Contributors"));

    aboutData.setOrganizationDomain(QByteArray("kde.org"));
    aboutData.setProductName(QByteArray("whale"));

    aboutData.addAuthor(xi18nc("@info:credit", "Carl Schwan"),
            xi18nc("@info:credit", "Developer"),
            "carl@carlschwan.eu");

    KAboutData::setApplicationData(aboutData);
    
    app.setApplicationVersion(aboutData.version());
    app.setApplicationName(aboutData.componentName());
    
    QCommandLineParser parser;

    aboutData.setupCommandLine(&parser);
    parser.process(app);
    aboutData.processCommandLine(&parser);

    // QML initialisation
    QQmlApplicationEngine engine;
    
    
    
    
    const char *uri = "org.kde.whale";
    qmlRegisterType<KFilePlacesModel>(uri, 1, 0, "FilePlacesModel");
    qmlRegisterType<DirModel>(uri, 1, 0, "DirModel");
    qmlRegisterSingletonType<DirModelUtils>(uri, 1, 0, "DirModelUtils", [=](QQmlEngine *, QJSEngine *) {
        return new DirModelUtils;
    });
    
    qmlRegisterType<FilePlacesModel>(uri, 1, 0, "FilePlacesModel");

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));

    // Set default path for file dialog
    // setFolder(QUrl::fromLocalFile(QStandardPaths::writableLocation(QStandardPaths::HomeLocation)));

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
