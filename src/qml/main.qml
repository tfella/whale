/*
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.1
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC2
import org.kde.kirigami 2.13 as Kirigami

Kirigami.ApplicationWindow {
    id: root
    globalDrawer: PlacesGlobalDrawer {
        modal: !root.wideScreen
        onModalChanged: drawerOpen = !modal
        contentItem.implicitWidth: Kirigami.Units.gridUnit * 10
        onPlaceOpenRequested: {
            filePicker.folder = place;

        }
    }
    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.None

    header: QQC2.ToolBar {
        implicitHeight: Kirigami.Units.gridUnit * 1.8 + Kirigami.Units.smallSpacing * 2

        contentItem: RowLayout {
            Item {
                id: leftHandleAnchor
                visible: (typeof applicationWindow() !== "undefined" && applicationWindow().globalDrawer && applicationWindow().globalDrawer.enabled && applicationWindow().globalDrawer.handleVisible &&
                applicationWindow().globalDrawer.handle.handleAnchor == leftHandleAnchor) &&
                (globalToolBar.canContainHandles || (breadcrumbLoader.pageRow.firstVisibleItem &&
                breadcrumbLoader.pageRow.firstVisibleItem.globalToolBarStyle == Kirigami.ApplicationHeaderStyle.ToolBar))
                
                Layout.preferredWidth: height
            }
            QQC2.Label { text: "jiorejo"; }
            
            Kirigami.ActionToolBar {
                actions: [
                    Kirigami.Action {
                        icon.name: "folder"
                        text: i18n("Create folder")
                        visible: !root.selectExisting

                        onTriggered: filePicker.createDirectorySheet.open()
                    },
                    Kirigami.Action {
                        id: filterAction
                        icon.name: "view-filter"
                        checkable: true
                        text: i18n("Filter filetype")
                        checked: true
                    }
                ]
            }
        }
    }
    pageStack.initialPage: DirectoryPage {
        title: "rejio"

        id: filePicker
        folder: "~"
    }

    Component {
        id: initialPage
        Kirigami.Page { }
    }
}
