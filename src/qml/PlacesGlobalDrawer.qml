// SPDX-FileCopyrightText: 2020 Linus Jahn <lnj@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12 as Controls
import org.kde.kirigami 2.8 as Kirigami
import org.kde.whale 1.0

/**
 * The PlacesGlobalDrawer type provides a GlobalDrawer containing common places on the file system
 */
Kirigami.OverlayDrawer {
    id: root

    signal placeOpenRequested(url place)

    handleClosedIcon.source: null
    handleOpenIcon.source: null
    width: Math.min(applicationWindow().width * 0.8, Kirigami.Units.gridUnit * 13)

    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0

    contentItem: Controls.ScrollView {
        ListView {
            spacing: 0
            model: FilePlacesModel {
                id: filePlacesModel
            }

            section.property: "group"
            section.delegate: Kirigami.Heading {
                topPadding: Kirigami.Units.smallSpacing
                leftPadding: Kirigami.Units.largeSpacing
                level: 6
                text: section
                opacity: 0.7
            }

            delegate: Kirigami.AbstractListItem {
                required property string display
                required property string iconName
                required property bool hidden
                required property url url

                visible: !hidden
                width: parent.width
                contentItem: Row {
                    Kirigami.Icon {
                        source: iconName
                        width: height
                        height: Kirigami.Units.iconSizes.small
                    }
                    Controls.Label {
                        text: display
                    }
                }
                separatorVisible: false
                onClicked: {
                    root.placeOpenRequested(url);
                }
            }
        }
    }
}
