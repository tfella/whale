/*
 * SPDX-FileCopyrightText: (C) 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.1
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC2
import org.kde.kirigami 2.13 as Kirigami

import org.kde.whale 1.0 as Whale

Kirigami.GlobalDrawer {
    id: globalDrawer
    
    modal: Kirigami.Settings.isMobile
    collapsible: true
    collapsed: Kirigami.Settings.isMobile
    
    contentItem: QQC2.ScrollView {
        anchors.fill: parent
        implicitWidth: Math.min(Kirigami.Units.gridUnit * 20, globalDrawer.parent.width * 0.8)
        ListView {
            model: Whale.FilePlacesModel { }
            
            delegate: Kirigami.BasicListItem {
                label: model.display + " " + model.group
            }
            
            section.property: "group"
            section.delegate: Kirigami.ListSectionHeader {
                label: section
            }
        }
    }
}
